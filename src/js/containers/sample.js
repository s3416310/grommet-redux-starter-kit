// eslint-disable-next-line no-unused-vars
import React from 'react';
import TodoList from '../components/TodoList';

import { connect } from 'react-redux';
// eslint-disable-next-line no-unused-vars
import { addTodo, toggleTodo } from '../actions/todo';

/**
 * A container component is an interface between the actual presentation component
 * and the store. Its only purpose is to feed the presentation component with the props
 * it needs from the store, or to dispatch actions to the store. Presentation components
 * are located in js/components.
 */

/**
 * Each property of your presentation component can be listed here. If you need any
 * data from the app store, take it from state.something, where 'something' is your
 * reducer name or property/slice of the app store. See more in the reducer section
 * or look for combineReducers
 */
const mapStateToProps = (state, ownProps) => {
  return {
    todos: []
  };
};

/**
 * The only purpose of this object is to make dispatch available so any event
 * handlers for your component can dispatch using redux, so the stores can update as well.
 * Again, all the properties listed here must be properties of your presentation component located
 * in js/components. Remember, presentation components do not do anything 'logical', they just render
 * whatever is given to it.
 */
const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    todoClickHandler: (id) => {
      dispatch(toggleTodo(id));
    }
  };
};

/* Connect automatically generates a container component for your presentation component */
/* Use the container component to build your user interface */
export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
