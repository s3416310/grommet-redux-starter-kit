const ADD_TODO = 'ADD_LINK';
const TOGGLE_TODO = 'TOGGLE_TODO';

export const actionTypes = {
  ADD_TODO,
  TOGGLE_TODO
};

/**
 * These are action creators, they are synchronous and should return a JSON object with
 * atleast one property type='ACTION_ID', so that your reducer will catch the ACTION_ID
 * and change the state of the app. You will notice each ACTION_ID is a constant declared above
 * so refactoring becomes easier.
 */
export const addTodo = (text) => {
  return {
    type: actionTypes.ADD_TODO,
    text
  };
};
export const toggleTodo = (id) => {
  return {
    type: actionTypes.TOGGLE_TODO,
    id
  };
};
