// eslint-disable-next-line no-unused-vars
import React from 'react'

const Todo = ({clickHandler, text, completed}) => (
    <li onClick={clickHandler} style={{textDecoration: completed ? 'line-through':'none'}}>
        {text}
    </li>
)
/**
 * This react element accepts a list of todo's and a click handler
 */
const TodoList = ({todos, todoClickHandler}) => (
    <div className='todoList'>
         <ul>
            {
                todos.map((item) => {
                    return <Todo key={item.id} clickHandler={() => todoClickHandler(item.id)} {...item} />
                })
            }
        </ul>
    </div>

)
export default TodoList