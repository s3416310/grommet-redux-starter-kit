// eslint-disable-next-line no-unused-vars
import React from 'react';
import { combineReducers, createStore } from 'redux';

/**
 * This file is special, as its the one place where all reducers come together
 * All reducers combine to form one big app reducer, which then forms one big app store.
 * Redux handles this for us :)
 * Just import your reducer and add it to the list below.
 * If your reducer is called 'xyz', you can access your portion of the data from the store
 * as store.xyz.
 *
 */

const appStore = createStore(combineReducers({
  // add your reducer here as key: value, your reducer must be a function that accepts
  // (state, action)
}));

export default appStore;
