// eslint-disable-next-line no-unused-vars
import React from 'react';
import { actionTypes } from '../actions/sample';

/**
 * This is a sample reducer
 */
const sample = (state = [], action) => {
  switch (action.type) {
    case actionTypes.ADD_TODO:
      let { text, completed = false } = action;
      return [
        ...state,
        {
          id: state.length + 1,
          text,
          completed
        }
      ];
    case actionTypes.TOGGLE_TODO:
      return state.map((todo) => {
        if (todo.id === action.id) {
          return Object.assign({}, todo, {
            completed: !todo.completed
          });
        }
        return todo;
      });

    default:
      return state;
  }
};
export default sample;
