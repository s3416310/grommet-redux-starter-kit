import '../scss/index.scss';
// eslint-disable-next-line no-unused-vars
import React, { Component } from 'react';
// eslint-disable-next-line no-unused-vars
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './reducers/index';

// Grommet imports
import App from 'grommet/components/App';
import Box from 'grommet/components/Box';
import Anchor from 'grommet/components/Anchor';
import Header from 'grommet/components/Header';
import Footer from 'grommet/components/Footer';
import Title from 'grommet/components/Title';
import TodoAppDashboard from './components/TodoAppDashboard';

class Main extends Component {
  render () {
    return (
      // the provider gives store access to all container components
      <Provider store={store}>
        <App centered={false}>
          <Box full={true}>
            <Header direction="row" justify="between"
              pad={{horizontal: 'medium'}}>
              <Title>Kit</Title>
            </Header>
            <TodoAppDashboard />
            <Footer primary={true} appCentered={true} direction="column"
              align="center" pad="small" colorIndex="grey-1">
              <p>
                Build your ideas with <Anchor href="http://grommet.io"
                target="_blank">Grommet</Anchor>!
              </p>
            </Footer>
          </Box>
      </App>
      </Provider>
    );
  }
};

let element = document.getElementById('content');
ReactDOM.render(React.createElement(Main), element);

document.body.classList.remove('loading');
