# Grommet full project

This project has an todo list with a dashboard that illustrates basic usage
of grommet.

Dependencies

  1. Grommet UI Dependencies - https://grommet.github.io/docs/get-started
  2. Install the grommet CLI via ```$ npm install -g grommet-cli```
  3. Gulp installed globally ```$ npm install -g gulp```

(Optional) Use n to manage your node version, Grommet requires Node 4.4.x+ and NPM 3.0.x+
     https://www.npmjs.com/package/n

To run this application, execute the following commands:

  1. Install NPM modules

    ```
    $ npm install
    ```

  2. Start the development server:

    ```
    $ gulp dev
    ```

  3. Create the app distribution to be used by the back-end server

    ```
    $ gulp dist
    ```
